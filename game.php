<?php 


function getArrCards(){
	
	$path = dirname(__FILE__)."/images/front/";
	
	$arrImages = scandir($path);
	$arrCars = array();
	foreach($arrImages as $image){
		
		if($image == "." || $image == "..")
			continue;
		
		$info = pathinfo($image);
		
		$filename = $info["basename"];
		$name = $info["filename"];
		
		$arrCard  = array();
		$arrCard["name"] = $name;
		$arrCard["filename"] = $filename;
		$arrCard["url"] = "images/front/{$filename}";
		
		$arrCars[] = $arrCard;
		$arrCars[] = $arrCard;
		
	}
	
	shuffle($arrCars);
		
	return($arrCars);
}

/**
 * Poto CSS
 */
function putCss(){
	
	?>
	
	.field{
		background-color:darkred;
		padding:50px;
		min-height:300px;
		min-width:300px;
	}
	
	.card{
		width:130px;
		height:180px;
		float:left;
		background-color:white;
		margin:18px;
		border:6px solid gray;
		position:relative;
	}
	
	.card.ok{
		border-color:lawngreen;
	}
	
	.card .back,
	.card .front{
		width:100%;
		height:100%;
		background-image:url('images/back/backCard.png');
		background-size:cover;
		position:absolute;
		display:block;
		cursor:pointer;
	}
	
	.card .front{
		display:none;
	}
	
	.card.show-front .front{
		display:block;
	}
	
	.card.show-front .back{
		display:none;
	}
	
	
	<?php 
}

/**
 * Poto HTML
 */
function putGame(){
	
	$arrCards = getArrCards();
	
	?>
	<html>
	<head>
		<style>
			<?php putCss()?>
		</style>

<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		
		<script src="js/script.js"></script>
  		
	</head>
	
	<body>
		<h1><center>EL INFERNAL MEMORY DE LA MUERTE</center></h1>
		<div class="field">
			
			<?php foreach($arrCards as $card):?>
			
			<div class="card card-name-<?php echo $card["name"]?>" data-name="<?php echo $card["name"]?>">
				
				<div class="front" style="background-image:url('<?php echo $card["url"]?>')"></div>
				<div class="back"></div>
			</div>
							
			<?php endforeach?>
			
			<div style="clear:both;"></div>				
			
		</div>
	</body>
	</html>
	
	<?php 
	
}


putGame();
